#VERSION := $(shell ./scripts/get-version.sh)
VERSION := 0.1.0
REPOSITORY := cloud.yantra.oss/opa-enabled-confluent-kafka

clean:
	rm -fr target

build: jar image

image:
	sed s/VERSION/$(VERSION)/g Dockerfile.in > Dockerfile
	docker build -t cloud.yantra.oss/opa-enabled-confluent-kafka:$(VERSION) -t cloud.yantra.oss/opa-enabled-confluent-kafka:latest .

jar:
	./scripts/build.sh

push: build
	docker push $(REPOSITORY):$(VERSION)
	docker push $(REPOSITORY):latest
