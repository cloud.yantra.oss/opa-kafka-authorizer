#!/usr/bin/env bash

DIR="$( cd "$(dirname "$0")" ; pwd -P )"

docker run -w /app -v $DIR/..:/app hseeberger/scala-sbt:11.0.10_1.4.8_2.13.5 sbt clean package
