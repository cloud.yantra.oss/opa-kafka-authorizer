/** ****************************************************************************
  * Compilation related
  * ****************************************************************************
  */
scalaVersion := "2.13.5"
scalacOptions in (Compile, compile) ++= Seq(
  "-target:jvm-1.8",
  "-unchecked",
  "-deprecation",
  "-encoding",
  "utf8",
  "-feature",
  "-Ywarn-dead-code"
)

scalacOptions in (Compile, doc) ++= Seq("-groups", "-implicits")

/** ****************************************************************************
  * Library related configurations
  * ****************************************************************************
  */
lazy val `opa-kafka-authorizer` = (project in file("."))
  .settings(
    name := "OPA Kafka Authorizer",
    organization := "cloud.yantra.oss",
    version := "0.1.0",
    libraryDependencies ++= Seq(
      "org.apache.kafka" % "kafka_2.13" % "2.7.0"
    )
  )

apiURL := Some(url("https://gitlab.com/cloud.yantra.oss/opa-kafka-authoriser"))

logLevel := sbt.Level.Warn
