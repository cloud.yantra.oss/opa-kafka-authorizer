# kafka-authorizer

This directory contains a Kafka authorizer plugin that uses OPA to enforce policy decisions.

## Build

To build the plugin, you must have Docker installed. 

Run `make build` to build and package the plugin.

If you want to build without Docker `sbt clean package` should be enough but requires that you have Java
and `sbt` installed on your system. The JARs required to run the plugin will be written to `./target/scala-2.13/opa-kafka-authorizer_2.13-<VERSION>.jar`.

## Install

Include the following JAR files in the Kafka classpath:
- `opa-kafka-authorizer-<VERSION>.jar`

Enable the plugin by adding the following to `server.properties`:
```
authorizer.class.name: cloud.yantra.oss.opa.kafka.OpaAuthorizerClassic
```

The plugin supports the following properties:

| Property Key | Example | Description |
| --- | --- | --- |
| `opa.authorizer.url` | `http://opa:8181/v1/data/authz/allow` | Name of the OPA policy to query. |
| `opa.authorizer.allow.on.error` | `false` | Fail-closed or fail-open if OPA call fails. |
| `opa.authorizer.cache.initial.capacity` | `100` | Initial decision cache size. |
| `opa.authorizer.cache.maximum.size` | `100` | Max decision cache size. |
| `opa.authorizer.cache.expire.after.ms` | `600000` | Decision cache expiry in milliseconds. |
| `opa.authorizer.token` | `` | Token for authentication with OPA. |

## Usage

The OPA policy should return a boolean value indicating whether the action
should be allowed (`true`) or denied (`false`).

The plugin provides input data describing the principal, operation, and resource.

```hocon
# Example principle information.
input.session.principal.principalType = "User"
input.session.principal.name = "ANONYMOUS"
input.session.clientAddress = "127.0.0.1"
input.session.sanitizedUser = "ANONYMOUS"

# Example operation information.
input.operation.name = "ClusterAction"

# Example resource information.
input.resourceType.name = "Cluster"
input.resource.name = "kafka-cluster"
```

The following table summarizes the supported Kafka resource types and operation names.

| `input.resourceType.name` | `input.operation.name` |
| --- | --- |
| `Cluster` | `ClusterAction` |
| `Cluster` | `Create` |
| `Cluster` | `Describe` |
| `Group` | `Read` |
| `Group` | `Describe` |
| `Topic` | `Alter` |
| `Topic` | `Delete` |
| `Topic` | `Describe` |
| `Topic` | `Read` |
| `Topic` | `Write` |
