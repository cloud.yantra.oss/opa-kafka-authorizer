package cloud.yantra.oss.opa.kafka

import kafka.network.RequestChannel.Session
import kafka.security.auth.{Operation, Resource}

/**
  * Helper structure to encapsulate the input data for getting evaluated by OPA.
  * The policies will have access to all required information to correctly provide decision.
  * @param input
  */
case class AuthorizationPayload(
    input: AuthorizationPayloadInput
)
case class AuthorizationPayloadInput(
    operation: Operation,
    resource: Resource,
    session: Session
)
