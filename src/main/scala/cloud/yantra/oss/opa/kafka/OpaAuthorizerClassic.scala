package cloud.yantra.oss.opa.kafka

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}
import kafka.network.RequestChannel.Session
import kafka.security.auth.{Acl, Authorizer, Operation, Resource}
import org.apache.kafka.common.security.auth.KafkaPrincipal
import org.slf4j.{Logger, LoggerFactory}

import java.io.{BufferedReader, InputStreamReader, IOException, OutputStream}
import java.net.{HttpURLConnection, URL}
import java.util
import scala.collection.immutable

class OpaAuthorizerClassic extends Authorizer {
  val log: Logger = LoggerFactory.getLogger(classOf[OpaAuthorizerClassic])

  /**
    * Url to the service exposing the OPA engine.
    * Defaults to `http://localhost:8181/v1/data/authz/allow`
    */
  val OPA_AUTHORIZER_URL = "opa.authorizer.url"
  private var opaUrl: String = "http://localhost:8181/v1/data/authz/allow"

  /**
    * What should be the default behaviour, when some error is encountered during policy evaluation.
    * Defaults to `true`
    */
  val OPA_AUTHORIZER_DENY_ON_ERROR = "opa.authorizer.allow.on.error"
  private var allowOnError: Boolean = false

  /**
    * Token to authenticate/authorize with OPA service. Important when OPA service is running remotely.
    * If OPA service is running as side-car in Kubernetes cluster, then this can been ignored.
    * Defaults to empty string.
    */
  val OPA_AUTHORIZER_TOKEN = "opa.authorizer.token"
  private var opaToken: String = ""

  private val mapper: ObjectMapper = new ObjectMapper() with ScalaObjectMapper

  override def authorize(
      session: Session,
      operation: Operation,
      resource: Resource
  ): Boolean =
    allow(
      AuthorizationPayload(
        input = AuthorizationPayloadInput(
          session = session,
          operation = operation,
          resource = resource
        )
      )
    )

  override def configure(configs: java.util.Map[String, _]): Unit = {
    // Prepare the JSON parser for immutable objects
    mapper.registerModule(DefaultScalaModule)
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    log.debug("OPA Evaluation configs: {}", configs)

    // Checks for any value overrides for OPA services, and use them instead of the default ones.
    if (configs.containsKey(OPA_AUTHORIZER_URL)) {
      opaUrl = configs.get(OPA_AUTHORIZER_URL).asInstanceOf[String]
    }

    if (configs.containsKey(OPA_AUTHORIZER_DENY_ON_ERROR)) {
      allowOnError = configs.get(OPA_AUTHORIZER_DENY_ON_ERROR).asInstanceOf[Boolean]
    }

    if (configs.containsKey(OPA_AUTHORIZER_TOKEN)) {
      opaToken = configs.get(OPA_AUTHORIZER_DENY_ON_ERROR).asInstanceOf[String]
    }
  }

  override def addAcls(acls: Set[Acl], resource: Resource): Unit = ()

  override def removeAcls(acls: Set[Acl], resource: Resource): Boolean = false

  override def removeAcls(resource: Resource): Boolean = false

  override def getAcls(resource: Resource): Set[Acl] = Set.empty

  override def getAcls(
      principal: KafkaPrincipal
  ): immutable.Map[Resource, Set[Acl]] = immutable.Map.empty

  override def getAcls(): immutable.Map[Resource, Set[Acl]] =
    immutable.Map.empty

  override def close(): Unit = ()

  /**
    * Helper method to call the OPA service and get a decision.
    * The decision by be available from `opa` result in form of `true` or `false` value.
    *
    * @param payload
    * @return
    */
  private def allow(payload: AuthorizationPayload): Boolean =
    try {
      val conn: HttpURLConnection =
        new URL(opaUrl).openConnection.asInstanceOf[HttpURLConnection]
      conn.setDoOutput(true)
      conn.setRequestMethod("POST")
      // Harden to use JSON as payload
      conn.setRequestProperty("Content-Type", "application/json")
      if (!opaToken.isEmpty)
        conn.setRequestProperty("Authorization", "Bearer " + opaToken)
      val data: String = mapper.writeValueAsString(payload)
      val os: OutputStream = conn.getOutputStream
      os.write(data.getBytes)
      os.flush()

      // Its expected that the OPA policies will only return the decision, and nothing more.
      // Thus only first line is of interest.
      val br: BufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream))
      val map: util.HashMap[String, Boolean] = mapper.readValue(br.readLine, classOf[util.HashMap[String, Boolean]])
      br.close()
      val result = map.get("result")
      log.info(
        "Authorization evaluation -> Response code: {}, Authorized: {}, Request data: {}",
        conn.getResponseCode,
        result,
        data
      )
      result
    } catch {
      case e: IOException =>
        log.error(
          "Failure occurred while communicating with OPA service for request evaluation.",
          e
        )
        allowOnError
    }
}
