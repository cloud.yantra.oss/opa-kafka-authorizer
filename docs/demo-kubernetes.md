# Demo on Kubernetes Cluster

The demo can be executed on any kubernetes cluster. However, during drafting of this document, kubernetes cluster provided by Docker on developer's machine was used. 

The document assumes that the reader will have basic understanding of kubernetes. Pre-requisite:
- `helm`
- `kubectl`
- Any kubernetes cluster

### Fetching the helm charts from confluent
Let’s start by cloning the repository and updating the dependencies.
```shell
git clone git@github.com:confluentinc/cp-helm-charts.git
cd cp-helm-charts
#helm dependency update charts/cp-kafka/
```
The last command updates the dependencies in the `cp-kafka` chart that has a dependency of `cp-zookeeper` chart. Installation of `cp-kafka` fails without running the update command.

### Deploy Kafka 
Deploy Kafka brokers with Zookeepers with a release name (e.g. `confluent`) using the below command:
```shell
helm install confluent ./charts/cp-kafka
```
It will take a few minutes before all the pods start running. Let’s verify the resources created with our release are working fine using `kubectl`.

```shell
kubectl get all
```

It should provide an output similar to below:
```shell
NAME                           READY   STATUS    RESTARTS   AGE
pod/confluent-cp-kafka-0       2/2     Running   0          72m
pod/confluent-cp-kafka-1       2/2     Running   0          70m
pod/confluent-cp-kafka-2       2/2     Running   0          70m
pod/confluent-cp-zookeeper-0   2/2     Running   0          72m
pod/confluent-cp-zookeeper-1   2/2     Running   0          70m
pod/confluent-cp-zookeeper-2   2/2     Running   0          70m

NAME                                      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)             AGE
service/confluent-cp-kafka                ClusterIP   10.99.111.102   <none>        9092/TCP,5556/TCP   72m
service/confluent-cp-kafka-headless       ClusterIP   None            <none>        9092/TCP            72m
service/confluent-cp-zookeeper            ClusterIP   10.99.42.177    <none>        2181/TCP,5556/TCP   72m
service/confluent-cp-zookeeper-headless   ClusterIP   None            <none>        2888/TCP,3888/TCP   72m
service/kubernetes                        ClusterIP   10.96.0.1       <none>        443/TCP             48d

NAME                                      READY   AGE
statefulset.apps/confluent-cp-kafka       3/3     72m
statefulset.apps/confluent-cp-zookeeper   3/3     72m
```

You will notice all brokers and zookeepers have 2 containers per pod, one of these is the `prometheus` container. You can disable `prometheus` by editing the values files or simply setting values from Helm command-line while installing (E.g. `helm install --set prometheus.jmx.enabled=false`)

### Deploy Kafka Client Pod

Execute the below command to deploy a client pod in the kubernetes cluster. 
```shell
 kubectl create deployment kafka-client  --image confluentinc/cp-enterprise-kafka:6.0.1 --replicas 1 -- sh -c "exec tail -f /dev/null"
```
> Note: If you have other means to access the kafka cluster, and freedom to expose the kafka service, then you may not require to create a separate client pod. 

If you execute the command `kubectl get pod`, you should get the client pod listed as below:
```shell
NAME                            READY   STATUS    RESTARTS   AGE
kafka-client-54c478d7f4-64d6c   1/1     Running   0          7m7s
```
> Note: the pod name, the pod names are generated randomly by Kubernetes and it will be different in your cluster. 
> The rest of this document will use this pod name for executing commands.  

You can use the below command to login into the cline pod, and execute queries on Kafka:
```shell
kubectl exec -it kafka-client-54c478d7f4-64d6c -- /bin/bash 
```

Once logged in you can execute following test commands:
```shell
## Delete preexisting kafka topic
kafka-topics --bootstrap-server confluent-cp-kafka-opa-headless:9092 --topic confluent-cp-kafka-topic --delete --if-exists
## Create kafka topic
kafka-topics --bootstrap-server confluent-cp-kafka-opa-headless:9092 --topic confluent-cp-kafka-topic --create --partitions 1 --replication-factor 1 --if-not-exists

### Send test message to topic
echo "some random text message" | kafka-console-producer --bootstrap-server confluent-cp-kafka-opa-headless:9092 --topic confluent-cp-kafka-topic 

### Fetch the message from topic
 kafka-console-consumer --bootstrap-server confluent-cp-kafka-opa-headless:9092 --topic confluent-cp-kafka-topic --from-beginning --timeout-ms 5000
```
> Note: In case you are getting timeout, try increase the timeout parameter. Depending on kubernetes cluster. e.g. local docker based cluster, the processing could be slow. 

